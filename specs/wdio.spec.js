const { expect, assert } = require('chai');

const wait = (ms) =>{
  return new Promise(
    resolve => setTimeout(resolve, ms || 1000)
  );
};

const rundomNumber = () => Date.now();

const inputText = async (element, value, timeout = 5000) => {
  await element.waitForDisplayed({ timeout });
  await element.setValue(value);
};

const clickButton = async (element, timeout = 5000) => {
  await element.waitForDisplayed({ timeout });
  await element.click();
};

const waitRedirectTo = async (expectedUrl) => {
  await browser.waitUntil(
    async function () {
      const url = await browser.getUrl();
      return url === expectedUrl;
    },
    { timeout: 5000 },
  ); 
}

const signIn = async (testData) => {
  const emailField =  await $('//input[@name="email"]');
  const passwordField =  await $('//input[@name="password"]');
  const button = await $('//button');

  await inputText(emailField, testData.email);
  await inputText(passwordField, testData.password);

  await clickButton(button);
}

const USERS = {
  valid: {
    email: "marcus-shmarkus@gmail.com",
    password: "Pa55word",
  },
  invalidPassword: {
    email: "marcus-shmarkus@gmail.com",
    password: "password",
  },
  invalidEmail: {
    email: "shmarkus@gmail.com",
    password: "Pa55word",
  },
  admin1: {
    email: "john_admin1@admin.com",
    password: "Pa55word",
  },
  admin2: {
    email: "john_admin2@admin.com",
    password: "Pa55word",
  },
};

describe('Registration:', function () {
  it('should be able to register', async function () {

    await browser.setWindowSize(1440, 960);
    await browser.url('/sign-up');

    const usernameField = await $('input[name="name"]');
    const surnameField = await $('input[name="surname"]');
    const birthDateField = await $('input[name="birthdate"]');
    const emailField = await $('input[name="email"]');
    const passwordField = await $('input[name="password"]');
    const retryPasswordField = await $('input[name="retypePassword"]');
    const phoneField = await $('input[name="phone"]');

    const ddls = await $$('div.selectStyles__control');

    const genderDdl = ddls[0];
    const statusDdl = ddls[1];

    const femaleOption = await $('div.selectStyles__option=female');
    const doctorOption = await $('div.selectStyles__option=doctor');

    const signUpButton = await $('button');

    await usernameField.waitForDisplayed({ timeout: 5000 });
    await usernameField.setValue('Marcus');

    await surnameField.waitForDisplayed({ timeout: 5000 });
    await surnameField.setValue('Aurelius');

    await birthDateField.waitForDisplayed({ timeout: 5000 });
    await birthDateField.setValue('11/11/1999');

    await emailField.waitForDisplayed({ timeout: 5000 });
    await emailField.setValue(`marcus${rundomNumber()}@gmail.com`);

    await passwordField.waitForDisplayed({ timeout: 5000 });
    await passwordField.setValue('Pa55word');

    await retryPasswordField.waitForDisplayed({ timeout: 5000 });
    await retryPasswordField.setValue('Pa55word');

    await phoneField.waitForDisplayed({ timeout: 5000 });
    await phoneField.setValue('380000000000');

    await genderDdl.waitForDisplayed({ timeout: 5000 });
    await genderDdl.click();

    await femaleOption.waitForDisplayed({ timeout: 5000 });
    await femaleOption.click();

    await statusDdl.waitForDisplayed({ timeout: 5000 });
    await statusDdl.click();

    await doctorOption.waitForDisplayed({ timeout: 5000 });
    await doctorOption.click();

    await signUpButton.waitForDisplayed({ timeout: 5000 });
    await signUpButton.click();

    await browser.waitUntil(
      async function () {
        const url = await browser.getUrl();
        return url === 'http://46.101.234.121/doctors';
      },
      { timeout: 5000 },
    );

    const url = await browser.getUrl();
    expect(url).to.be.eql('http://46.101.234.121/doctors');
    
    await browser.reloadSession();
  });
});

describe('Sign In:', function () {
  it('Positive. Should be able to sign in with valid credentials.', async function () {
    // GIVEN
    await browser.setWindowSize(1440, 960);
    await browser.url('/sign-in');

    const testData = USERS.valid;
    const expectedData = { url: "http://46.101.234.121/doctors" };

    // WHEN
    await signIn(testData);
    await browser.waitUntil(
      async function () {
        const url = await browser.getUrl();
        return url === expectedData.url;
      },
      { timeout: 5000 },
    );

    // THEN
    const url = await browser.getUrl();
    assert.equal(url, expectedData.url, "Expected URL");

    await browser.reloadSession();
  });

  const fixturesNegative = [
    {
      description: "Invalid email",
      testData: USERS.invalidEmail,
      expectedData: {
        errorTitle: "Error 401",
        errorText: "Email is incorrect",
        url: "http://46.101.234.121/sign-in",
      },
    },
    {
      description: "Invalid password",
      testData: USERS.invalidPassword,
      expectedData: {
        errorTitle: "Error 401",
        errorText: "Password is incorrect",
        url: "http://46.101.234.121/sign-in",
      },
    },
  ];
  fixturesNegative.forEach( ({ description, testData, expectedData } = fixture) => {
    it(`Negative. ${description}`, async () => {
      // GIVEN
    await browser.setWindowSize(1440, 960);
    await browser.url('/sign-in');

    const elements = {
      errorTitle: await $('//div[@class="rrt-title"]'), // alternative '//div[starts-with(@id, "dialogTitle")]'
      errorText: await $('//div[@class="rrt-text"]'), //alternative '//div[starts-with(@id, "dialogDesc")]'
    };
    // WHEN
    await signIn(testData);

    // THEN
    const errorTitle = await elements.errorTitle.getText();
    const errorText = await elements.errorText.getText();
    const url = await browser.getUrl();

    assert.equal(errorTitle, expectedData.errorTitle, "error title");
    assert.equal(errorText, expectedData.errorText, "error text");
    assert.equal(url, expectedData.url, "Expected URL");
    
    await browser.reloadSession();

    });
  });
});


describe('Change users profile:', function () {
  const fixtures = [
    {
      description: "Change name",
      credentials: USERS.valid,
      testData: {
        name: `Mark${rundomNumber()}`,
      }
    },
  ];
  fixtures.forEach( ({ description, testData, credentials } = fixture) => {
    it(`Positive. Should be able to change profile\'s info. ${description}`, async () => {
      // GIVEN
    await browser.setWindowSize(1440, 960);
    await browser.url('/sign-in');

    const elements = {      
      myProfile: await $('//a[text()="My Profile"]'),
      profileView: {
        title: await $('//span[@class="styles_title__1FkvS"]'),
        edit: await $('//button[contains(@class, "styles_edit__ftuHl")]'),
        name: await $('//span[@class="styles_name__2vTNE"]'),
      },
      profileEdit: {
        title: await $('//h2[@class="styles_title__3VzLp"]'),
        nameField: await $('//input[contains(@class, "styles_textInput__3q_vf") and @name="name"]'),
        editButton: await $('//button[@type="submit" and text()="Edit"]'),
        
      },

    };

    // WHEN
    await signIn(credentials);
    await waitRedirectTo("http://46.101.234.121/doctors");


    // go to my profile page
    await elements.myProfile.click();
    await elements.profileView.title.waitForDisplayed({ timeout: 5000 });
    
    // go to edit profile
    await clickButton(elements.profileView.edit);
    await elements.profileEdit.title.waitForDisplayed({ timeout: 5000 });

    // make changes
    await inputText(elements.profileEdit.nameField, testData.name);
    await clickButton(elements.profileEdit.editButton);
    await elements.profileView.title.waitForDisplayed({ timeout: 5000 });

    // THEN
    await wait(500);
    const actual = {
      name: await elements.profileView.name.getText(),
    };

    assert.equal(actual.name.split(" ")[0], testData.name, "New name");
    await browser.reloadSession();
    });
  });
});
